#ifndef CANTRONIK_INCLUDE_FILE
#define CANTRONIK_INCLUDE_FILE

#include "sja1000.h"

typedef struct canpar_device
{
	struct pardevice *parport;        // diese Struktur wird von den parport_... Funktionen ben�tigt
	int ecp;                      // 0=Bidirektional oder EPP 1=ECP
	int open;                                // 0 = nicht in Verwendung     1 = in Verwendung
	int num;                                 // Ergebnis von "register_can_bus_device()" wird hier gespeichert
	sja1000_isr pIsr;
	struct sja1000_admin *pSja1000Par;
	unsigned char statusport;
} CANPARDEVICE, *PCANPARDEVICE;


#endif /* CANTRONIK_INCLUDE_FILE */
