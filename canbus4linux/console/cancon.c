/*
 *	Testprogramm for CANBUS4LINUX driver
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>    // wegen "sleep()", open(), close()
#include <sys/ioctl.h>

#include "../canbus4linux.h"

int device=-1;

int close_can_device(int file) 
{
	if(file != -1)
	{
		close(file);
	}
	return 1;
}

int open_can_device(char *name)
{
	int file;

	file = open(name,O_RDWR);
	if(file == -1)
	{
		printf("can't open canbus4linux device\n");
	}
	else
	{
		fcntl(file,F_SETOWN,getpid());
		fcntl(file,F_SETFL, FASYNC | fcntl(file,F_GETFL));
	}

	return file;
}

int transmit_can_message(int file, int id, int rtr, int dlc, unsigned char *msg)
{
	struct canbus_transmit_data d;

	d.fmt = 0;
	d.identifier = id;
	d.rtr = rtr;
	d.dlc = dlc;
	if(msg)
		memcpy(d.msg,msg,dlc);

	return ioctl(file, CANBUS4LINUX_WRITE_TRANSMIT_DATA, &d);
}

int acceptance_filter_code(int file, unsigned long val)
{
	int ret;
	struct canbus_acceptance_filter d;

	ret = ioctl(file, CANBUS4LINUX_READ_ACCEPTANCE_FILTER, &d);
	if(ret < 0) return ret;
	d.code = val;
	return ioctl(file, CANBUS4LINUX_WRITE_ACCEPTANCE_FILTER, &d);
}

int acceptance_filter_mask(int file, unsigned long val)
{
	int ret;
	struct canbus_acceptance_filter d;

	ret = ioctl(file, CANBUS4LINUX_READ_ACCEPTANCE_FILTER, &d);
	if(ret < 0) return ret;
	d.mask = val;
	return ioctl(file, CANBUS4LINUX_WRITE_ACCEPTANCE_FILTER, &d);
}

int main(int argc, char *argv[])
{
	int a,s=0,x,t,l=0,id=0,rtr=0;
	unsigned char m[8];
	char devname[50];
	
	//printf("cancon\n");
	strcpy(devname,"/dev/canbus4linux");
	for(t=1;t<argc;t++)
	{
		if(!strcmp(argv[t],"-d") && (argc > (t+1)))
			strcpy(devname,argv[t+1]);
		if((argv[t][0] == '-') || (argv[t][0] == '?'))
		{
			if((argv[t][1] == 'h') || (argv[t][1] == '?') || (argv[t][0] == '?'))
			{
				printf("cancon\n"\
					   "  -h or -? or ? show this help\n"\
					   "  -b <baudrate>\n"\
					   "  -c <acceptance code>\n"\
					   "  -m <acceptance mask>\n"\
					   "  -i <can identifier>\n"\
					   "  -r <rtr>\n"\
					   "  -x <can mode 0=2.0A  1=2.0B>\n"\
					   "  -o <command (see CANBUS_CMD_... in canbus4linux.h)\n"\
					   "  -s cancon will send the can message\n"\
					   "  -n 1...8 data bytes\n");
				return 0;
			}
		}
	}
	device = open_can_device(devname);
	if(device >= 0)
	{
		ioctl(device, CANBUS4LINUX_INIT, 0);
		for(t=1;t<argc;t++)
		{
			if(!strcmp(argv[t],"-b") && (argc > (t+1)))
				ioctl(device, CANBUS4LINUX_SET_BAUDRATE, atoi(argv[t+1]));
			if(!strcmp(argv[t],"-c") && (argc > (t+1)))
			{
				sscanf(argv[t+1],"%x",&x);
				acceptance_filter_code(device, x);
			}
			if(!strcmp(argv[t],"-m") && (argc > (t+1)))
			{
				sscanf(argv[t+1],"%x",&x);
				acceptance_filter_mask(device, x);
			}
			if(!strcmp(argv[t],"-i") && (argc > (t+1)))
			{
				sscanf(argv[t+1],"%x",&id);
			}
			if(!strcmp(argv[t],"-r") && (argc > (t+1)))
				rtr = atol(argv[t+1]);
			if(!strcmp(argv[t],"-x") && (argc > (t+1)))
				ioctl(device, CANBUS4LINUX_SET_CAN_MODE, atoi(argv[t+1]));
			if(!strcmp(argv[t],"-o") && (argc > (t+1)))
				ioctl(device, CANBUS4LINUX_SET_COMMAND, atoi(argv[t+1]));
			if(!strcmp(argv[t],"-s"))
				s=1;
			if(!strcmp(argv[t],"-n"))
			{
				l = argc-t-1;
				if(l > 8) l = 8;
				//printf("argc=%d t=%d dlc=%d\n",argc,t,l);
				for(x=0;x<l;x++)
				{
					sscanf(argv[t+x+1],"%x",&a);
					m[x] = a;
					//printf("m[%d] = %02x\n",x,m[x]);
				}
			}
		}
		if(s)
		{
			transmit_can_message(device, id, rtr, l, m);
		}
		close_can_device(device);
		return 0;
	}
	return 10;
}