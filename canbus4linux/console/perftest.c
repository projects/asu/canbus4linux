#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>    // wegen "sleep()", open(), close()
#include <sys/ioctl.h>

#include "../canbus4linux.h"


/***********************************************************************

     Set a link to you can device, for example:

     ln -s /dev/can0 /dev/can

 ***********************************************************************/




int file;

void SignalFromDriver(int signum)
{
    struct canbus_event data;
    while(1)
    {
        if (ioctl(file, CANBUS4LINUX_READ_EVENT_DATA, &data) <= 0)
        {
            break;
        }
	}
	return;
}
char* devname="/dev/can";

int main(int argc, char *argv[])
{
	if(argc>2 && !strcmp(argv[1],"-d"))
		devname = argv[2];

    file = open(devname,O_RDWR);
    if(file == -1)
    {
        printf("can't open canbus4linux device\n");
    }
    else
    {
	   	struct sigaction sa;
		struct canbus_transmit_data data;
		int t;

	    // install signal handler
		memset(&sa,0,sizeof(sa));
		sa.sa_handler = SignalFromDriver;
		sa.sa_flags = SA_RESTART;
		sigaction(SIGIO,&sa,0);

        // Because of following 2 lines, a signal is generated on every event 
        // (receiving, transmitting, errors, warnings,...)
        fcntl(file,F_SETOWN,getpid());
        fcntl(file,F_SETFL, FASYNC | fcntl(file,F_GETFL));

	    ioctl(file, CANBUS4LINUX_INIT, 0);
	    ioctl(file, CANBUS4LINUX_SET_BAUDRATE, 100000);
	    ioctl(file, CANBUS4LINUX_SET_CAN_MODE, CANBUS_FORMAT_CAN_2_0_B);
	    ioctl(file, CANBUS4LINUX_SET_DEFAULT_FRAME_FORMAT, CANBUS_TRANS_FMT_EXT);
	    for(t=0;t<10000;t++)
	    {
			data.fmt = 0;
			data.identifier = t;
			data.rtr = 0;
			data.dlc = 8;
			data.msg[0] = 0x11;
			data.msg[1] = 0x22;
			data.msg[2] = 0x33;
			data.msg[3] = 0x44;
			data.msg[4] = 0x55;
			data.msg[5] = 0x66;
			data.msg[6] = 0x77;
			data.msg[7] = 0x88;
			ioctl(file, CANBUS4LINUX_WRITE_TRANSMIT_DATA, &data);
		}
    }

    if(file != -1)
    {
        close(file);
    }

	return 0;
}