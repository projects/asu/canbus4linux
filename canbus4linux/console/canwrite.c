/*
 *	Testprogramm for CANBUS4LINUX driver
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>    // wegen "sleep()", open(), close()
#include <sys/ioctl.h>

#include "../canbus4linux.h"


int main(int argc, char *argv[])
{
	FILE *f;
	int t;
	char *device="/dev/can0";

	for(t=1;t<argc;t++)
	{
		if(!strcmp(argv[t],"-d") && (argc > (t+1)))
		{
			device = argv[t+1];
		}
		else if(!strcmp(argv[t],"-b") && (argc > (t+1)))
		{
			f = fopen("/proc/canbus/0/baudrate","w");
			if(!f)
			{
				printf("no canbus device registered\n");
				return 10;
			}
			fprintf(f,argv[t+1]);
			fclose(f);
		}
	}

	f = fopen(device,"w");
	if(f)
	{
		for(t=1;t<1000;t++)
		{
			fprintf(f,"%x 0 1 12\n",t);
			//fflush(f);
		}
		fclose(f);
	}

	return 0;
}