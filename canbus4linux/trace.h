// Debug-Ausgaben
#ifdef TRACE
#undef TRACE
#endif

#define DEBUG	0

#define CANBUS4LINUX "CANBUS4LINUX: "

#if DEBUG
//#define TRACE(__x__...) do {printk(CANBUS4LINUX "%s [%d]:  ",__FILE__,__LINE__);printk(__x__); printk("\n");} while (0)
#define TRACE(__x__...) do {printk(CANBUS4LINUX "[%d]:  ",__LINE__);printk(__x__); printk("\n");} while (0)
#else
#define TRACE(__x__...) do {} while (0)
#endif

//#define INFO_TRACE(__x__...) do {printk(KERN_INFO "%s:  ",__FILE__);printk(__x__); printk("\n");} while (0)
#define INFO_TRACE(__x__...) do {printk(CANBUS4LINUX);printk(__x__); printk("\n");} while (0)
