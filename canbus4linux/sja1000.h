#ifndef SJA1000_INCLUDE_FILE
#define SJA1000_INCLUDE_FILE

#include "canbus4linux.h"

typedef int  (*sja1000_openCanDevice)		(void *pSpecificPar);
typedef int  (*sja1000_closeCanDevice)		(void *pSpecificPar);
typedef void (*sja1000_writeToRegister)		(void *pSpecificPar, u8 addr, u8 value);
typedef void (*sja1000_writeToRegisterRR)	(void *pSpecificPar, u8 addr, u8 value);
typedef u8   (*sja1000_readFromRegister)	(void *pSpecificPar, u8 addr);
typedef u8   (*sja1000_readFromRegisterRR)	(void *pSpecificPar, u8 addr);

struct sja1000_admin;
typedef int  (*sja1000_isr)				(void *pSpecificPar, struct sja1000_admin *pSja1000Par);
typedef int  (*sja1000_registerIsr)		(void *pSpecificPar, sja1000_isr pIsr, struct sja1000_admin *pSja1000Par);
typedef int  (*sja1000_unregisterIsr)	(void *pSpecificPar);


struct sja1000_access
{
	sja1000_openCanDevice		pOpenCanDevice;
	sja1000_closeCanDevice		pCloseCanDevice;
	sja1000_writeToRegister		pWriteToRegister;
	/*sja1000_writeToRegisterRR	pWriteToRegisterRR;*/ // this function is obsolete (you don't need it in a hardware driver)
	sja1000_readFromRegister	pReadFromRegister;
	/*sja1000_readFromRegisterRR	pReadFromRegisterRR;*/ // this function is obsolete (you don't need it in a hardware driver)
	sja1000_registerIsr			pRegisterIsr;
	sja1000_unregisterIsr		pUnregisterIsr;
	int							bCanChipsetFlags;  // show Flags: CANBUS_CSF_...
	int							chipset_frequency;
	u8							output_control_register;
};

// internal administration
struct sja1000_admin
{
	struct sja1000_access access;
	int in_use; // 0=released 1=used
	void *pDeviceParm;
	struct canbus_admin *pCanBusParm;
	int canbus_admin_number;
	char cDeviceName[MAX_DEVICE_NAME_LENGTH];
	int bCan_2B; // 0=CAN 2.0 A   1=CAN 2.0B
	unsigned long iBaudrate;
	//unsigned long iAcceptanceCode;
	//unsigned long iAcceptanceMask;
	canbus_isr isr;
	spinlock_t irq_lock;	// used to disable irq handling for the chip
	unsigned long lock_flags;
	spinlock_t transmit_lock;
	unsigned long transmit_lock_flags;
};

extern int sja1000_register_device(char *name, int version, void *pSpecificPar, struct sja1000_access *access, int prefered_min, int prefered_max );
extern int sja1000_unregister_device(int can_num);

#endif /* SJA1000_INCLUDE_FILE */
