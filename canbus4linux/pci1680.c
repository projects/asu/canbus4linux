/*
 * pci1680.c
 * Copyright (c) 2003 Kirill Smelkov <smelkov@mph1.phys.spbu.ru>
 *
 * ... BLURB ABOUT HARDWARE...
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/*
 * two words about prefixes:
 * board_ prefix marks routines assotiated with the whole board
 * pci1680_ prefix marks routines assotiated with some chip on the board
 */

/*
 * modify for PCI1680 by Pavel Vaynerman <pv@etersoft.ru>
 * ( tested for kernel 2.6.12 )
 */

#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/ioport.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,5,67)
#include <linux/interrupt.h>
#endif
#include <asm/io.h>

#include "trace.h"
#include "canbus4linux.h"
#include "pci1680.h"

#ifndef SA_INTERRUPT
#define SA_INTERRUPT 0x20000000
#endif


#ifdef MODULE_LICENSE
MODULE_LICENSE("GPL");
#endif

struct board_info {
	const char *name;
	long Fosc;			// oscillator frequency
	u8   OCR;			// OCR (output control register) value specific for board
	unsigned long pci_base[MAXCHIPS];
};

static const struct board_info boards_list[] = {
//  name		Fosc,			OCR,	pci_base{chip1,chip2,...}
{	"pci1680",	16000000,	0xfa,	/* {2,3} */{ PCI_BASE_ADDRESS_2,PCI_BASE_ADDRESS_3 } },	// channels of PCI1680
{	NULL	}
};

#define MAXBOARDS	1
static pci1680_BOARD devices[MAXBOARDS];
//static int DevId=(CAN_PCI_VENDOR_ID<<0x10)|CAN_PCI_DEVICE_ID;

// *******************
// * REGISTER ACCESS *
// *******************
static void pci1680_writereg(void * data, u8 reg, u8 val)
{
	pci1680_CHIP * self = (pci1680_CHIP *) data;
	TRACE("PCI1680: writereg(0x%2.2x,0x%2.2x)", reg, val);
	outb(val,self->io_base+reg);
//	*(unsigned char*)phys_to_virt(self->io_base+reg)=val;
}

static u8 pci1680_readreg(void * data, u8 reg)
{
	pci1680_CHIP* self = (pci1680_CHIP *) data;
#ifdef DEBUG
	u8 val;
	val = inb(self->io_base+reg);
	TRACE("PCI1680: readreg(0x%2.2x) = 0x%2.2x", reg, val);
	return val;
#else
	return inb(self->io_base+reg);
#endif	
}

// ****************
// * IRQ HANDLING *
// ****************
static int pci1680_register_isr(void * data, sja1000_isr chip_isr, struct sja1000_admin * chip_isr_data)
{
	pci1680_CHIP * self = (pci1680_CHIP *) data;

	TRACE("PCI1680: register_isr()");

	if (!self)
		return -EINVAL;

	self->chip_isr		= chip_isr;
	self->chip_isr_data	= chip_isr_data;

	return 0;
}

static int pci1680_unregister_isr(void * data)
{
	pci1680_CHIP * self = (pci1680_CHIP *) data;

	TRACE("PCI1680: unregister_isr()");

	if (!self)
		return -EINVAL;

	self->chip_isr		= 0;
	self->chip_isr_data	= 0;

	return 0;
}

// the whole board interrupt service routine
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,19)
static irqreturn_t board_isr(int irq, void * dev_id)
#else
	#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,4)
	static irqreturn_t board_isr(int irq, void * dev_id, struct pt_regs * regs)
	#else
	static void board_isr(int irq, void * dev_id, struct pt_regs * regs)
	#endif
#endif
{
	pci1680_BOARD * self = (pci1680_BOARD *) dev_id;
	unsigned int i;

	TRACE("pci1680: isr...");
	if (!self)
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,4)
		return IRQ_NONE;
#else
		return;
#endif

/*	TRACE("pci1680: nchips=%d",self->nchips); */
	if ( self->nchips<=0 || self->nchips>MAXCHIPS )
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,4)
		return IRQ_HANDLED;
#else
		return;
#endif

	for (i=0;i<self->nchips;++i)
		if (self->chips[i].chip_isr)
			self->chips[i].chip_isr(&self->chips[i],self->chips[i].chip_isr_data);

/*	TRACE("pci1680: finish isr.."); */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,4)
		return IRQ_HANDLED;
#else
		return;
#endif
}

// *****************
// *  OPEN & CLOSE *
// *****************
static int pci1680_open(void * data)
{
	pci1680_CHIP * self = (pci1680_CHIP *) data;
	int err;

	TRACE("PCI1680: open()");
//	spin_lock(&self->lock);
	do {
		if (self->open) {
			err = -EBUSY;
			break;
		}

		self->open++;

		// NOTE: this is currently specific for my card (the wierd thing)
		//       hardware reset is made via
//		pci1680_writereg(self, 0x1e, 0x00);		// reset

	} while (0);
//	spin_unlock(&self->lock);

	try_module_get(THIS_MODULE);
	return 0;
}

static int pci1680_close(void * data)
{
//	pci1680_CHIP * self = (pci1680_CHIP *) data;
	TRACE("close()");
	module_put(THIS_MODULE);
	return 0;
}

// * MODULE INIT & CLEANUP *
// *************************

void __exit pci1680_cleanup(void);

int __init pci1680_init(void)
{
	int err=-ENODEV;
	int num, i;
	struct pci_dev* dev = NULL;

	TRACE("PCI1680: init()");

	if((dev=pci_find_device(CAN_PCI_VENDOR_ID, CAN_PCI_DEVICE_ID, dev)))
	{
		TRACE("find CAN pci device");
//		TRACE("Name : %s", dev->pretty_name);
		TRACE("IRQ: %d", dev->irq);
		if( pci_enable_device(dev) )
		{
			TRACE("Couldn't enable pci dev");
			return 1;
		}
	}
	else
	{
		TRACE("No PCI1680 card found !\n");
		return 1;
	}

	if (dev->irq==0) {
		TRACE("PCI1680 irq=0 ?!");
		err = 1;
		goto out;
	}

	/* default init */
	for( num=0; num<MAXBOARDS; num++ )
	{
		devices[num].dev = NULL;
		devices[num].io_irq = 0;
		devices[num].nchips = -1;
	}

	for (num=0; ( boards_list[num].name!=NULL && num<MAXBOARDS ) ;++num)
	{
		pci1680_BOARD* pdev = &devices[num];
		const struct board_info* idev = &boards_list[num];
		int address;

		struct sja1000_access access = {
			pOpenCanDevice:				pci1680_open,
			pCloseCanDevice:			pci1680_close,
			pWriteToRegister:			pci1680_writereg,
			pReadFromRegister:			pci1680_readreg,
			pRegisterIsr:				pci1680_register_isr,
			pUnregisterIsr:				pci1680_unregister_isr,
			bCanChipsetFlags:			CANBUS_CFS_CAN_2_0_B | CANBUS_CFS_EXT_FRAME,
			chipset_frequency:			idev->Fosc,
			output_control_register:	idev->OCR
		};

		pdev->io_irq = dev->irq;
		pdev->dev = dev;
		pdev->nchips = 0;

		TRACE("requesting irq...");
		err = request_irq(dev->irq, board_isr, IRQF_SHARED  /* SA_INTERRUPT */, "pci1680", pdev);
		if (err) {
			TRACE("request_irq(%i) failed", dev->irq);
			goto out;
		}

		// io for all chips an the board
		TRACE("registering chips...");
		for( i=0; i<MAXCHIPS; i++ )
		{
			pci1680_CHIP* chip = &pdev->chips[i];
		
			snprintf(chip->name, MAX_DEVICE_NAME_LENGTH, "%s[%i]", idev->name, i);

			chip->num	= -1;
			chip->lock	= SPIN_LOCK_UNLOCKED;

//			chip->io_base = pci_resource_start(dev,idev->pci_base[i]);
			pci_read_config_dword(dev, idev->pci_base[i], &address);
			if( address == 0xffffffff )
			{
				TRACE("Can't get the address2 of this card!\n");
				return 1;
			}

			chip->io_base 	= address & PCI_BASE_ADDRESS_IO_MASK;

			// register every chip on the board in the canbus4linux subsystem
			TRACE("%s: registering device no.: %i ...", idev->name, i);
			chip->num = sja1000_register_device(chip->name, CANBUS4LINUX_VERSION, chip, &access, 0, MAXCHIPS);
			if (chip->num == -1) {
				TRACE("sja1000_register_device() failed");
				err = -ENOMEM;
				goto out; // not enough memory to register this device
			}
			
			pdev->nchips++;
		}

		err = 0;
	}


out:
	if (err)
		pci1680_cleanup();

	return err;
}

void __exit pci1680_cleanup(void)
{
	int num, i;

	TRACE("PCI1680 cleanup()");	

	for( num=0; num<MAXBOARDS; ++num )
	{
		pci1680_BOARD* pdev = &devices[num];
		
		if ( !pdev->dev )
			continue;

		if( pdev->nchips>0 && pdev->nchips<=MAXCHIPS )
		{
			for (i=0; i<pdev->nchips; ++i) {
				pci1680_CHIP* chip = &pdev->chips[i];

				if (chip && chip->num !=-1 )
					chip->num = sja1000_unregister_device(chip->num);
			}
		}

		free_irq(pdev->dev->irq,pdev);
		pci_disable_device(pdev->dev);
	}
}

module_init(pci1680_init);
module_exit(pci1680_cleanup);
MODULE_AUTHOR("Pavel Vainerman <pv@etersoft.ru>");
MODULE_DESCRIPTION("(canbus4linux): CAN driver for PCI1680 can board");
