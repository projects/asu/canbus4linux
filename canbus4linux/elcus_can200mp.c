/*
 * elcus_can200mp.c
 * Copyright (c) 2003 Kirill Smelkov <smelkov@mph1.phys.spbu.ru>
 *
 * ... BLURB ABOUT HARDWARE...
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/*
 * two words about prefixes:
 * board_ prefix marks routines assotiated with the whole board
 * elcus_can200mp_ prefix marks routines assotiated with some chip on the board
 */

#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/ioport.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,5,67)
#include <linux/interrupt.h>
#endif
#include <asm/io.h>

#include "trace.h"
#include "canbus4linux.h"
#include "isa_sja1000.h"

#ifdef MODULE_LICENSE
MODULE_LICENSE("GPL");
#endif

struct io_info {
	unsigned long io_base;
	unsigned long io_len;
};

struct board_info {
	const char *name;
	int  io_irq;		// irq line
	long Fosc;			// oscillator frequency
	u8   OCR;			// OCR (output control register) value specific for board
	int  nchips;
	struct io_info io_info[MAXCHIPS];
};

#define MAXBOARDS	1
static ISA_SJA1000_BOARD devices[MAXBOARDS];

// *******************
// * REGISTER ACCESS *
// *******************
static void elcus_can200mp_writereg(void * data, u8 reg, u8 val)
{
	ISA_SJA1000_CHIP * self = (ISA_SJA1000_CHIP *) data;
	TRACE("writereg(0x%2.2x,0x%2.2x)", reg, val);

	outb(val, self->io_base + reg);
}

static u8 elcus_can200mp_readreg(void * data, u8 reg)
{
	ISA_SJA1000_CHIP * self = (ISA_SJA1000_CHIP *) data;
	u8 val;

	val = inb(self->io_base + reg);
	TRACE("readreg(0x%2.2x) = 0x%2.2x", reg, val);

	return val;
}

// ****************
// * IRQ HANDLING *
// ****************
static int elcus_can200mp_register_isr(void * data, sja1000_isr chip_isr, struct sja1000_admin * chip_isr_data)
{
	ISA_SJA1000_CHIP * self = (ISA_SJA1000_CHIP *) data;

	TRACE("register_isr()");

	if (!self)
		return -EINVAL;

	self->chip_isr		= chip_isr;
	self->chip_isr_data	= chip_isr_data;

	return 0;
}

static int elcus_can200mp_unregister_isr(void * data)
{
	ISA_SJA1000_CHIP * self = (ISA_SJA1000_CHIP *) data;

	TRACE("unregister_isr()");

	if (!self)
		return -EINVAL;

	self->chip_isr		= 0;
	self->chip_isr_data	= 0;

	return 0;
}

// the whole board interrupt service routine
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,19)
static irqreturn_t board_isr(int irq, void * dev_id)
#else
	#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,4)
	static irqreturn_t board_isr(int irq, void * dev_id, struct pt_regs * regs)
	#else
	static void board_isr(int irq, void * dev_id, struct pt_regs * regs)
	#endif
#endif
{
	ISA_SJA1000_BOARD * self = (ISA_SJA1000_BOARD *) dev_id;
	unsigned int i;

	TRACE("isr()");
	if (!self)
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,4)
		return IRQ_NONE;
#else
		return;
#endif

	for (i=0;i<self->nchips;++i)
		if (self->chips[i].chip_isr)
			self->chips[i].chip_isr(&self->chips[i],self->chips[i].chip_isr_data);
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,4)
		return IRQ_HANDLED;
#else
		return;
#endif
}

// *****************
// *  OPEN & CLOSE *
// *****************
static int elcus_can200mp_open(void * data)
{
	ISA_SJA1000_CHIP * self = (ISA_SJA1000_CHIP *) data;
	int err;

	TRACE("open()");
//	spin_lock(&self->lock);
	do {
		if (self->open) {
			err = -EBUSY;
			break;
		}

		self->open++;

		// NOTE: this is currently specific for my card (the wierd thing)
		//       hardware reset is made via
		elcus_can200mp_writereg(self, 0x1e, 0x00);		// reset

	} while (0);
//	spin_unlock(&self->lock);
	
	return 0;
}

static int elcus_can200mp_close(void * data)
{
//	ISA_SJA1000_CHIP * self = (ISA_SJA1000_CHIP *) data;

	TRACE("close()");
	return 0;
}


static struct board_info cards[] = {
//  name           irq   Fosc,		OCR,  nchips	{io_base, io_len} for each chip
{	"can-200pc",	3,	16000000,	0x1b,	2,		{ {0x100, 0x20}, {0x120, 0x20} }	},
{	NULL	}
};

static int min_minor=0;
static int irq=3;
static int iobase1=0x100;
static int iobase2=0x120;

// *************************
// * MODULE INIT & CLEANUP *
// *************************

void __exit elcus_can200mp_cleanup(void);

int __init elcus_can200mp_init(void)
{
	int num, i, minor;
	int err = -ENODEV;

	cards[0].io_irq = irq;
	cards[0].io_info[0].io_base = iobase1;
	cards[0].io_info[1].io_base = iobase2;

	for (num=0,minor=0; (cards[num].name!=NULL && num<MAXBOARDS); ++num )
	{
		ISA_SJA1000_BOARD *       dev  = &devices[num];
		const struct board_info * idev = &cards[num];

		unsigned int	io_irq;

		struct sja1000_access access = {
			pOpenCanDevice:				elcus_can200mp_open,
			pCloseCanDevice:			elcus_can200mp_close,
			pWriteToRegister:			elcus_can200mp_writereg,
			pReadFromRegister:			elcus_can200mp_readreg,
			pRegisterIsr:				elcus_can200mp_register_isr,
			pUnregisterIsr:				elcus_can200mp_unregister_isr,
			bCanChipsetFlags:			CANBUS_CFS_CAN_2_0_B | CANBUS_CFS_EXT_FRAME,
			chipset_frequency:			idev->Fosc,
			output_control_register:	idev->OCR
		};

		// io for all chips an the board
		TRACE("requesting io...");
		for (i=0;i<idev->nchips;++i) {
			ISA_SJA1000_CHIP * chip = &dev->chips[i];

			unsigned long	io_base;
			unsigned long	io_len;

			snprintf(chip->name, MAX_DEVICE_NAME_LENGTH, "%s[%i]", idev->name, i);

			chip->num	= -1;
			chip->lock	= SPIN_LOCK_UNLOCKED;

			dev->nchips++;

			io_base = idev->io_info[i].io_base;
			io_len	= idev->io_info[i].io_len;

			TRACE("request_region(%lx,%lx)...", io_base, io_len);
			if (!request_region(io_base, io_len, chip->name)) {
				TRACE("request_region(%lx,%lx) failed", io_base, io_len);
				err=-EBUSY;
				goto out;
			}

			chip->io_base = io_base;
			chip->io_len  = io_len;

			++minor;
		}

		// irq for the board
		io_irq  = idev->io_irq;

		TRACE("requesting irq...");
		err = request_irq(io_irq, board_isr, IRQF_DISABLED /* IRQF_SHARED */ /* flags */, "elcus_can200mpcan", dev);
		if (err) {
			TRACE("request_irq(%i) failed", io_irq);
			goto out;
		}

		dev->io_irq = io_irq;

		// register every chip on the board in the canbus4linux subsystem
		for (i=0;i<dev->nchips;++i) {
			ISA_SJA1000_CHIP * chip = &dev->chips[i];

			TRACE("%s: registering device no.: %i ...", idev->name, i+min_minor);
			chip->num = sja1000_register_device(chip->name, CANBUS4LINUX_VERSION, chip, &access, min_minor, minor+min_minor);
			if (chip->num == -1) {
				TRACE("sja1000_register_device() failed");
				err = -ENOMEM;
				goto out; // not enough memory to register this device
			}
		}

		err = 0;
	}

out:
	if (err)
		elcus_can200mp_cleanup();

	return err;
}

void __exit elcus_can200mp_cleanup(void)
{
	int num, i;

	TRACE("cleanup()");
	for (num=0;num<MAXBOARDS;++num)
	{
		ISA_SJA1000_BOARD * dev = &devices[num];

		if (!dev->nchips)
			continue;

		for (i=0; i<dev->nchips; ++i) {
			ISA_SJA1000_CHIP * chip = &dev->chips[i];

			if (chip->num!=-1)
				chip->num = sja1000_unregister_device(chip->num);
		}
	
		if (dev->io_irq)
			free_irq(dev->io_irq, dev);

		for (i=0; i<dev->nchips; ++i) {
			ISA_SJA1000_CHIP * chip = &dev->chips[i];

			if (chip->io_base)
				release_region(chip->io_base, chip->io_len);
		}
	}
}

module_init(elcus_can200mp_init);
module_exit(elcus_can200mp_cleanup);
MODULE_AUTHOR("Pavel Vaynerman <pv@etersoft.ru>");
MODULE_DESCRIPTION("CAN driver for Elcus CAN-200MP card");

module_param(min_minor,int,0444);
MODULE_PARM_DESC(min_minor,"1i");

module_param(irq,int,0444);
MODULE_PARM_DESC(irq,"1i");

module_param(iobase1,int,0444);
MODULE_PARM_DESC(iobase1,"1i");

module_param(iobase2,int,0444);
MODULE_PARM_DESC(iobase2,"1i");
