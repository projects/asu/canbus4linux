#ifndef CANBUS_INCLUDE_FILE
#define CANBUS_INCLUDE_FILE

#define CANBUS4LINUX_VERSION (0)        /* Binary Version (this constant will in checked by some structures)  */
#define CANBUS4LINUX_VERSION_STR "0.3.1" 
#define MAX_DEVICE_NAME_LENGTH (100)


#define CANBUS4LINUX_PROC_DIR "canbus"  /* define for /proc filesystem (/proc/canbus) */


#define CANBUS_FORMAT_CAN_2_0_A (0)
#define CANBUS_FORMAT_CAN_2_0_B (1)

// Chipset Flags
#define CANBUS_CFS_CAN_2_0_A (1<<0)		/* Chipset can used for CAN 2.0 A */
#define CANBUS_CFS_CAN_2_0_B (1<<1)		/* Chipset can used for CAN 2.0 B */
#define CANBUS_CFS_EXT_FRAME (1<<2)		/* Chipset support extended frame format (only in CAN 2.0B mode) */
#define CANBUS_CFS_POLLING   (1<<3)     /* (not implemented in canbus driver) Chipset driver supports only polling (interrupts are default) */

// Events
enum t_canbus_events
{
	CANBUS_EVENT_RECEIVED=1,
	CANBUS_EVENT_TRANSMITTED,
	CANBUS_EVENT_BUS_ERROR,
	CANBUS_EVENT_WARNING,
	CANBUS_EVENT_LEAVING_STANDBY,
	CANBUS_EVENT_ARBITRATION_LOST,
	CANBUS_EVENT_OVERRUN,
	CANBUS_EVENT_PASSIVE,
	CANBUS_EVENT_ENTERING_STANDBY,
	CANBUS_EVENT_DEVICE_CHANGED,
	CANBUS_EVENT_END
};

// Commands
enum t_canbus_commands
{
	CANBUS_CMD_ENTER_STANDBY=1,
	CANBUS_CMD_ABORT_TRANSMISSION,
	CANBUS_CMD_CLEAR_OVERRUN,
	CANBUS_CMD_LEAVE_STANDBY,
	CANBUS_CMD_SELF_RECEPTION_REQUEST,
	CANBUS_CMD_LISTEN_ON,
	CANBUS_CMD_LISTEN_OFF,
	CANBUS_CMD_VIRTUALIZE_ON,
	CANBUS_CMD_VIRTUALIZE_OFF,


	CANBUS_CMD_LAST_ENTRY		/* this must be the last entry in the list */
};

#ifdef CANBUS4LINUX_CLEARTEXT_COMMANDS /* define this only in one file of a project !!! */
// Commands as plain text
const char *canbus4linux_commands[] = 
{
	"",
	"enter standby",
	"abort transmission",
	"clear overrun",
	"leave Standby",
	"self reception request",
	"listen on",
	"listen off",
	"virtualize on",
	"virtualize off",


	" - "  /* this must be the last entry in the list */
};
#endif



#define CANBUS_NUMBER_OF_CONSTANT_BAUDRATES (50)	/* max. 50 entries for constant baudrates */

// The time is a 64-bit number...
struct canbus_time
{
	unsigned long low;
	unsigned long high;
};

// Acceptance Filter (Hardware filter in SJA1000, or Softwarefilter)
struct canbus_acceptance_filter
{
	unsigned long code;
	unsigned long mask;
};

// Properties of the selected channel
struct canbus_properties
{
	unsigned long version; // version of canbus4linux: CANBUS4LINUX_VERSION
	char device_name[MAX_DEVICE_NAME_LENGTH];
	long min;	// min baudrate
	long max;	// max baudrate
	int number_commands; // number of "commands[]" entries
	int commands[CANBUS_CMD_LAST_ENTRY]; // supported commands of the selected channel (depends on mode)
	int number_baudrates; // 0 if "canbus_set_baudrate()" is supported, otherwise only "baudrates[]" are supported
	unsigned long baudrates[CANBUS_NUMBER_OF_CONSTANT_BAUDRATES];
	unsigned long chipset_flags; // see: CANBUS_CFS_...
	int number_registers; // for direct acces to the chipset from an application
};

struct canbus_register_access
{
    unsigned int address;
    unsigned int value;
};


#define CANBUS_TRANS_FMT_DEFAULT (0)             /* send this data with default frame format */
#define CANBUS_TRANS_FMT_STD (1)                 /* send this data in 11 bit format */
#define CANBUS_TRANS_FMT_EXT (2)                 /* send this data in 29 bit format */
// Transmission structure
// With this structure a CAN message will sended
struct canbus_transmit_data
{
    int    fmt; // see CANBUS_TRANS_FMT_...., ignore this in canbus_event-structure (see below)
	unsigned long	identifier;     // CAN Identifier
	unsigned char	rtr;			// Remote Transmission Request bit (0=CAN message with data, 1=request data from receiver)
	unsigned char	dlc;			// Data Length Code (= number of data bytes in "msg[]")
	unsigned char	msg[8];         // data bytes
};


struct canbus_event
{
	int event; // see: t_canbus_events
	struct canbus_time time;

	//----------- used, if event=CANBUS_ISR_RECEIVED | CANBUS_ISR_TRANSMITTED ---------------
    unsigned long lost;
	struct canbus_transmit_data data;

	//----------- used, if event=CANBUS_ISR_ARBITRATION_LOST ------------------------------
	int arbitration;
	
};


/* see also: /usr/src/linux/Documentation/ioctl-number.txt
 *     and:  /usr/src/linux/Documentation/devices.txt
 */
#define CANBUS4LINUX_IOC_MAGIC 'z'
#define CANBUS4LINUX_WRITE_ACCEPTANCE_FILTER  _IOW(CANBUS4LINUX_IOC_MAGIC, 0x80, struct canbus_acceptance_filter)
#define CANBUS4LINUX_READ_ACCEPTANCE_FILTER  _IOR(CANBUS4LINUX_IOC_MAGIC, 0x81, struct canbus_acceptance_filter)
#define CANBUS4LINUX_SET_BAUDRATE  _IO(CANBUS4LINUX_IOC_MAGIC, 0x82)
#define CANBUS4LINUX_GET_BAUDRATE  _IO(CANBUS4LINUX_IOC_MAGIC, 0x83)
#define CANBUS4LINUX_INIT  _IO(CANBUS4LINUX_IOC_MAGIC, 0x84)
#define CANBUS4LINUX_SET_BAUDRATE_BY_CONSTANT _IO(CANBUS4LINUX_IOC_MAGIC, 0x85)
#define CANBUS4LINUX_GET_BAUDRATE_BY_CONSTANT  _IO(CANBUS4LINUX_IOC_MAGIC, 0x86)
#define CANBUS4LINUX_SET_CAN_MODE  _IO(CANBUS4LINUX_IOC_MAGIC, 0x87)
#define CANBUS4LINUX_SET_COMMAND  _IO(CANBUS4LINUX_IOC_MAGIC, 0x88)
#define CANBUS4LINUX_WRITE_TRANSMIT_DATA  _IOW(CANBUS4LINUX_IOC_MAGIC, 0x89, struct canbus_transmit_data)
#define CANBUS4LINUX_READ_EVENT_DATA  _IO(CANBUS4LINUX_IOC_MAGIC, 0x90)
#define CANBUS4LINUX_READ_TIME  _IOR(CANBUS4LINUX_IOC_MAGIC, 0x91, struct canbus_time)
#define CANBUS4LINUX_SET_DEFAULT_FRAME_FORMAT  _IO(CANBUS4LINUX_IOC_MAGIC, 0x92)
#define CANBUS4LINUX_TEST_DEVICE  _IO(CANBUS4LINUX_IOC_MAGIC, 0x93)
#define CANBUS4LINUX_READ_PROPERTIES  _IO(CANBUS4LINUX_IOC_MAGIC, 0x94)
#define CANBUS4LINUX_RESET_TRANSMIT  _IO(CANBUS4LINUX_IOC_MAGIC, 0x96)

/* New commands: 28.01.2003
 */
#define CANBUS4LINUX_WRITE_SOFT_ACCEPTANCE_FILTER  _IOW(CANBUS4LINUX_IOC_MAGIC, 0x95, struct canbus_acceptance_filter)
#define CANBUS4LINUX_READ_SOFT_ACCEPTANCE_FILTER  _IOR(CANBUS4LINUX_IOC_MAGIC, 0x95, struct canbus_acceptance_filter)
/* New commands: 06.01.2005
 */
#define CANBUS4LINUX_WRITE_REGISTER  _IOW(CANBUS4LINUX_IOC_MAGIC, 0x8a, struct canbus_register_access)
#define CANBUS4LINUX_READ_REGISTER  _IOR(CANBUS4LINUX_IOC_MAGIC, 0x8a, struct canbus_register_access)

#ifdef __KERNEL__
struct canbus_admin;
struct canbus_file;
typedef int (*canbus_isr)			(void *pSpecificPar, struct canbus_admin *pPar, struct canbus_event *pEvent);
  
typedef int (*canbus_registerIsr)	(void *pSpecificPar, canbus_isr pIsr, struct canbus_admin *pSja1000Par);
typedef int (*canbus_unregisterIsr)	(void *pSpecificPar);
  
  
typedef int (*canbus_open_device)	(void *pSja1000Par);
typedef int (*canbus_close_device)	(void *pSja1000Par);
typedef int (*canbus_init_device)	(void *pSja1000Par);
typedef int (*canbus_set_baudrate)	(void *pSja1000Par, unsigned long baudrate);
typedef int (*canbus_get_properties)(void *pSja1000Par, struct canbus_properties *props);
typedef int (*canbus_set_register)	(void *pSja1000Par, int addresse, unsigned int value);
typedef int (*canbus_get_register)	(void *pSja1000Par, int addresse, int *value);
typedef int (*canbus_set_can_mode)	(void *pSja1000Par, int can_2b);
typedef int (*canbus_set_baudrate_by_constant)
									(void *pSja1000Par, unsigned long constant);
typedef int (*canbus_set_acceptance_filter)
									(void *pSja1000Par, struct canbus_acceptance_filter *filter);
typedef int (*canbus_set_command)	(void *pSja1000Par, int command);
typedef int (*canbus_transmit_data)	(void *pSja1000Par, struct canbus_transmit_data *trans);
typedef int (*canbus_test_device)	(void *pSja1000Par);



struct canbus_access
{
    canbus_registerIsr          pRegisterIsr;
    canbus_unregisterIsr        pUnregisterIsr;

    canbus_open_device          pOpen;
    canbus_close_device         pClose;
    canbus_init_device          pInit;
    canbus_set_baudrate         pSetBaudrate;
    canbus_get_properties       pGetProperty;
    canbus_set_register         pSetRegister;
    canbus_get_register         pGetRegister;
    canbus_set_can_mode         pSetCanMode;
    canbus_set_baudrate_by_constant pSetBaudrateByConstant;
    canbus_set_acceptance_filter pSetAcceptanceFilter;
    canbus_set_command          pSetCommand;
    canbus_transmit_data        pTransmitData;
    canbus_test_device          pTestDevice;
};

extern int canbus4linux_register_device(char *name, int version, void *pSpecificPar, struct canbus_access *access, int prefered_min, int prefered_max);
extern int canbus4linux_unregister_device(int can_num);

extern int canbus4linux_open_device(struct canbus_admin *pPar);
extern int canbus4linux_close_device(struct canbus_admin *pPar);
extern int canbus4linux_init_device(struct canbus_admin *pPar);
extern int canbus4linux_set_baudrate(struct canbus_admin *pPar, unsigned long baudrate);
extern unsigned long canbus4linux_get_baudrate(struct canbus_admin *pPar);
extern int canbus4linux_get_properties(struct canbus_admin *pPar, struct canbus_properties *props);
extern int canbus4linux_set_register(struct canbus_admin *pPar, int addresse, unsigned int value);
extern int canbus4linux_get_register(struct canbus_admin *pPar, int addresse, unsigned int *value);
extern int canbus4linux_set_can_mode(struct canbus_admin *pPar, int can_2b);
extern int canbus4linux_set_baudrate_by_constant(struct canbus_admin *pPar, unsigned long constant);
extern unsigned long canbus4linux_get_baudrate_by_constant(struct canbus_admin *pPar);
extern int canbus4linux_set_acceptance_filter(struct canbus_admin *pPar, struct canbus_acceptance_filter *filter);
extern struct canbus_acceptance_filter canbus4linux_get_acceptance_filter(struct canbus_admin *pPar);
extern int canbus4linux_set_command(struct canbus_admin *pPar, int command);
extern int canbus4linux_transmit_data(struct canbus_admin *pPar, struct canbus_transmit_data *trans, struct file *fa_file);
extern int canbus4linux_get_event(struct canbus_admin *pPar, struct canbus_event *trans, struct canbus_file *file);
extern int canbus4linux_set_default_frame_format(struct canbus_admin *pPar, int fmt);
extern int canbus4linux_test_device(struct canbus_admin *pPar);
extern int canbus4linux_set_soft_acceptance_filter(struct canbus_admin *pPar, struct canbus_acceptance_filter *filter);
extern struct canbus_acceptance_filter canbus4linux_get_soft_acceptance_filter(struct canbus_admin *pPar);
#endif



#endif /* CANBUS_INCLUDE_FILE */
