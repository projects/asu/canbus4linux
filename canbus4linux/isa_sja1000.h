#ifndef	ISA_SJA1000_H_
#define	ISA_SJA1000_H_

#include "sja1000.h"

typedef struct isa_sja1000_chip
{
	unsigned long	io_base;
	unsigned long	io_len;

	char					name[MAX_DEVICE_NAME_LENGTH];
	int						num;

	spinlock_t				lock;
	int						open;
	sja1000_isr				chip_isr;
	struct sja1000_admin *	chip_isr_data;
} ISA_SJA1000_CHIP;


#define	MAXCHIPS 4	// 4 sja1000 on each board
typedef struct isa_sja1000_board
{
	unsigned int	io_irq;
	unsigned int	nchips;
	ISA_SJA1000_CHIP	chips[MAXCHIPS];
} ISA_SJA1000_BOARD;

#endif

// vim: ts=4
