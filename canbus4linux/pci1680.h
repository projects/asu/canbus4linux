#ifndef	pci1680_H_
#define	pci1680_H_

#include "sja1000.h"
#include <linux/pci.h>

#define CAN_PCI_VENDOR_ID 0x13fe 	// Advantech vednor id
#define CAN_PCI_DEVICE_ID 0x1680 	// PCI-1680 device ID

typedef struct pci1680_chip
{
	unsigned long	io_base;
	char					name[MAX_DEVICE_NAME_LENGTH];
	int						num;

	spinlock_t				lock;
	int						open;
	sja1000_isr				chip_isr;
	struct sja1000_admin *	chip_isr_data;
} pci1680_CHIP;


#define	MAXCHIPS 2

typedef struct pci1680_board
{
	struct pci_dev* dev;
	unsigned int	io_irq;
	unsigned int	nchips;
	pci1680_CHIP	chips[MAXCHIPS];
} pci1680_BOARD;

#endif

// vim: ts=4
