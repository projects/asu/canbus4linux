#!/bin/sh

#kversion=2.6.32
#flavour=el-smp

kversion=2.6.27
flavour=std-def

#kversion=2.4.29
#flavour=std-up

. /usr/src/linux-$kversion-$flavour/gcc_version.inc

PWD=`pwd`
make -C /usr/src/linux-$kversion-$flavour SUBDIRS=$PWD V=1 modules
