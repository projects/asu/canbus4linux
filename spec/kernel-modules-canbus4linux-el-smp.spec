%define module_name	canbus4linux
%define module_release	alt7
%define module_version	0.4

%define kversion	2.6.32
%define krelease	alt12
%define flavour		el-smp

%define base_arch %(echo %_target_cpu | sed 's/i.86/i386/;s/athlon/i386/')

%define module_dir /lib/modules/%kversion-%flavour-%krelease/kernel/drivers/char/canbus4linux

Summary:	Linux %{module_name} for CAN devices
Name:		kernel-modules-%{module_name}-%flavour
Version:	%module_version
Release:	%module_release
License:	Restricted
Group:		System/Kernel and hardware

Packager:       Pavel Vainerman <pv@altlinux.ru>

ExclusiveOS:	Linux
BuildPreReq:	kernel-build-tools >= 0.7
BuildRequires:	modutils
BuildRequires:	perl
BuildRequires:	rpm >= 4.0.2-75
BuildRequires:  kernel-headers-modules-%flavour = %kversion-%krelease
BuildRequires:  kernel-source-%module_name = %module_version

Provides:  kernel-modules-%module_name-%kversion-%flavour-%krelease = %version-%release
Conflicts: kernel-modules-%module_name-%kversion-%flavour-%krelease < %version-%release
Conflicts: kernel-modules-%module_name-%kversion-%flavour-%krelease > %version-%release

Prereq:		coreutils
Prereq:		modutils
Prereq:         kernel-image-%flavour = %kversion-%krelease
Requires(postun): kernel-image-%flavour = %kversion-%krelease
ExclusiveArch:	%ix86

%description
Driver for CAN devices

%prep
%__rm -rf kernel-source-%module_name-%module_version
%__tar -jxvf %_usrsrc/kernel/sources/kernel-source-%module_name-%module_version.tar.bz2
%setup -D -T -n kernel-source-%module_name-%module_version

%build
. %_usrsrc/linux-%kversion-%flavour/gcc_version.inc

mv -f Makefile Makefile.orig

# Generate Makefile
cat << EOF > Makefile
obj-m += can200par.o cantronik.o elektor_canpar.o isa_sja1000.o sja1000.o canbuscore.o pci1680.o elcus_can200mp.o fastwel_cancpc108.o
EOF

%make TEMP_DIR=`pwd` -C %_usrsrc/linux-%kversion-%flavour/ V=1 modules SUBDIRS=`pwd`

%install
%__mkdir -p $RPM_BUILD_ROOT/%module_dir
%if "%kversion" <= "2.6.0"
    %__cp -a %module_name.o $RPM_BUILD_ROOT/%module_dir
%else
    %__cp -a *.ko $RPM_BUILD_ROOT/%module_dir
%endif

%post
# %%post_kernel_modules %kversion-%flavour-%krelease

%postun
# %%postun_kernel_modules %kversion-%flavour-%krelease

%files
%defattr(644,root,root,755)
%module_dir

%changelog
* Sat Dec 24 2005 Pavel Vainerman <pv@altlinux.ru> 5.4.1-alt0.1
- initial build
