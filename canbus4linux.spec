%define module_name canbus4linux
%define module_version 0.5
%define module_release eter2
%define module_source_name canbus4linux

#### MODULE SOURCES ####
Name:          %module_name
Version:       %module_version
Release:       %module_release
Summary:       Linux %module_name modules sources
License:       GPL
Group:         Development/Kernel
BuildArch:     noarch
Source:        %module_source_name-%module_version.tar

Packager:      Pavel Vainerman <pv@altlinux.ru>

%package -n %module_name-devel
Summary: Headers and development files of %module_name library
Group: Development/GNOME and GTK+

%description -n %module_name-devel
Headers and development files of %module_name library

%package -n kernel-source-%module_name
Summary:       Linux %module_name modules sources
Group:         Development/Kernel

%description -n kernel-source-%module_name
%module_name modules sources for Linux kernel

%description
%module_name modules sources for Linux kernel

%prep
%setup -q -n %module_source_name-%module_version

%install
mkdir -p %buildroot%_usrsrc/kernel/sources/kernel-source-%module_name-%module_version/
mkdir kernel-source-%module_name-%module_version

cp *.c *.h COP* DOC* INST* TODO Makefile kernel-source-%module_name-%module_version/

tar -c kernel-source-%module_name-%module_version | bzip2 -c > \
    %buildroot%_usrsrc/kernel/sources/kernel-source-%module_name-%module_version.tar.bz2

%__mkdir -p %buildroot%_includedir/%module_name
cp -a canbus4linux.h %buildroot%_includedir/%module_name

%files -n %module_name-devel
%_includedir/%module_name/*

%files -n kernel-source-%module_name
%_usrsrc/kernel/sources/kernel-source-%module_name-%module_version.tar.bz2

%changelog
* Fri Mar 16 2012 Pavel Vainerman <pv@altlinux.ru> 0.5-eter2
- (canbuscore): increased the buffer size for messages (1000)

* Fri Mar 04 2011 Pavel Vainerman <pv@etersoft.ru> 0.5-eter1
- fixed bug in canbuscore.c (build new version)

* Mon Jan 24 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.4-eter8
- build to Etersoft addons
- fix license to GPL
- merge with 2.6.32 branch:
   code of pci1680.c build for 2.6.x kernels CONFIG_PCI_LEGACY used or
   supports for the deprecated pci_find_slot() and pci_find_device() APIs.

* Sat Oct 03 2009 Vitaly Lipatov <lav@altlinux.ru> 0.4-eter5
- build with latest sources
- add build elcus and fastwel modules

* Sat Jul 04 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-eter1
- build with git.eter
- replace subpackage canbus4linux-devel from module build to sources
- rename from source package kernel-source-canbus4linux to canbus4linux

* Wed Oct 08 2008 Pavel Vainerman <pv@altlinux.ru> 0.3-alt0.1
- Initial build
